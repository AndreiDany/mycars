
 * COMANDA GIT: Să se creeze un repository GIT nou local
 * git init

 * COMANDA GIT: Să se pună fișierul README.md în repository local 
 * git add Readme.md
 * git commit -m "readme in rep local"

 * COMANDA GIT: Să se pună repository local în repository remote pe serverul GitLab
 * git remote add origin git@gitlab.com:AndreiDany/mycars.git
 * git push --set-upstream origin master
 * git push 

* COMANDA GIT: Să se pună fișiere corsa.md si astra.md în repository local 
* git init
 * git add corsa.md
 * git add astra.md
 * git commit -m "corsa.md si astra.md in rep local"

 * COMANDA GIT: Să se pună modificările din repository local în repository remote
 * git remote add origin git@gitlab.com:AndreiDany/mycars.git
 * git push --set-upstream origin master
 * git push

 * COMANDA GIT: Să se pună modificarile la fișierul README.md în repository local 
 * git add Readme.md
 * git commit -m "readme in rep local2"

 * COMANDA GIT: Să se pună modificările din repository local în repository remote 
